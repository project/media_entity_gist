<?php

namespace Drupal\media_entity_gist\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks if a Gist is publicly visible.
 *
 * @Constraint(
 *   id = "GistIsPrivate",
 *   label = @Translation("Is gist private", context = "Validation"),
 *   type = { "entity", "entity_reference", "string", "string_long" }
 * )
 */
class GistIsPrivateConstraint extends Constraint {

  /**
   * The default violation message.
   *
   * @var string
   */
  public $message = 'This is not a valid gist url.';
  public $notFoundMessage = 'This gist id is not valid';

}
