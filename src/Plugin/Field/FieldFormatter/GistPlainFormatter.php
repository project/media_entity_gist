<?php

namespace Drupal\media_entity_gist\Plugin\Field\FieldFormatter;

use Github\Client;
use Drupal\Core\Template\Attribute;
use Drupal\Core\Field\FormatterBase;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\media_entity_gist\Plugin\media\Source\Gist;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'media_entity_gist_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "media_entity_gist_plain_formatter",
 *   label = @Translation("Gist plain formatter"),
 *   field_types = {
 *     "media_entity_gist_field"
 *   }
 * )
 */
class GistPlainFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * Github api client.
   *
   * @var \Github\Client
   */
  protected $client;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    Client $client
  ) {
    parent::__construct($plugin_id, $plugin_definition,  $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->client = $client;
  }


  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('media_entity_gist.php_github_api')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      $matches = [];

      if (!preg_match(Gist::$regex, $item->value, $matches)) {
        return ['#markup' => $this->t('Not a valid gist url')];
      }

      if (!empty($matches['user']) && !empty($matches['id'])) {
        $response = [];
        try {
          $response = $this->client->api('gist')->show($matches['id']);
        }
        catch (\RuntimeException $ex) {
          continue;
        }
        $files = $response['files'];
        unset($response["files"]);

        foreach ($files as $key => $file) {
          $ext = basename($file['filename']);
          $ext = explode('.', $ext);
          $ext = array_pop($ext);
          $files[$key]['attributes'] = new Attribute();
          $files[$key]['attributes']['class'] = [
            'gist-plain-code',
            implode('-', ['language', $ext]),
          ];
        }

        $parse = UrlHelper::parse($item->value);

        if (isset($parse['query']) && isset($parse['query']['file'])) {
          foreach ($files as $id => $file) {
            if ($id !== $parse['query']['file']) {
              unset($files[$id]);
            }
          }
        }

        $element[$delta] = [
          '#type' => 'container',
          'childs' => [
            'files' => [
              '#theme' => 'media_entity_gist_plain',
              '#meta' => $response,
              '#files' => $files,
            ],
          ],
        ];
      }
    }
    return $element;
  }

}
