CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------

The media entity gist module add a new media source for embed gist in your site.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/media_entity_gist

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/media_entity_gist

REQUIREMENTS
------------

This module requires the following modules:

 * Github PHP Api (https://github.com/KnpLabs/php-github-api)
 * Drupal core media module

RECOMMENDED MODULES
-------------------

 * Prism (https://www.drupal.org/project/prism):

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.

CONFIGURATION
-------------

Add a new media type with a gist source.

TROUBLESHOOTING
---------------

FAQ
---

MAINTAINERS
-----------

Current maintainers:
 * Erik Seifert - https://www.drupal.org/u/erik-seifert

 This project has been sponsored by:
 * b-connect GmbH
   Visit https://www.b-connect.de for more information.
